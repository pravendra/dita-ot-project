# DITA-OT Documentation
The DITA Open Toolkit, or DITA-OT for short, is a set of Java-based, open-source tools that provide processing for content authored in the Darwin Information Typing Architecture.
 
## To Achieve This will be the following steps

* [Dita-ot download](#download)
* [Installing Dita-ot in the Systems](#install)
* [Set new DITA-OT AEM instance](#dita-otAEM)
* [Creating Maps and Topics](#mapTopics)
* [Installing Custom css pulgin in DITA-OT](#pulginInstall)
* [Creating Profile](#profile)
* [Output of DITA-OT](#output)

## <a name=download ></a> Dita-ot download : [Link to download dita-ot zip file](http://www.dita-ot.org/download) 
## Installing ANT in the System

* Download ANT form the         given link [ANT DOWNLOAD](https://ant.apache.org/bindownload.cgi).

* Download ant verison 1.10.2.zip.

* After downloading Set the enviornment for ANT.
    * go to ControlPanel >> System .
    * Click on Advanced System Setting .
    * Click on Environment Variables.
    * CLick on New and type `ANT_HOME = <ANT Dir>`.
    * Click on Paths and edit and add >> `% ANT_HOME%\bin`.
    * After setting the enviornment variable goto the command prompt and type `ant -version`.

## <a name=install ></a> Installing Dita-ot in the Systems

* *For Mac OSX or Linux*

    Verify that the JAVA_HOME environment variable is set.
    
    `export JAVA_HOME=<JRE_dir>`
    
    Verify that the ANT_HOME environment variable is been set.

    `export ANT_HOME=<Ant_dir>`

    Verify that the PATH environment variable includes the Java and Ant executable files.
    
    `export PATH=$JAVA_HOME/bin:$ANT_HOME/bin:$PATH`

    Set the DITA_HOME environment variable to point to the DITA-OT installation directory.

    `export DITA_HOME=<DITA-OT_dir>`

* *For Windows*
    
    Verify that the JAVA_HOME environment variable is set.
    
    `java -version`
    
    Verify that the ANT_HOME environment variable is been set.

    `ant -version`

    Set the DITA_HOME environment variable to point to the DITA-OT installation directory.

    * go to ControlPanel >> System 
    * Click on Advanced System Setting 
    * Click on Environment Variables
    * CLick on New and type `DITA_HOME = <DITA Dir>`.
    * Click on Paths and edit and add >> `% DITA_HOME%\bin`.
    * After setting the enviornment variable goto the command prompt and type `dita -version`.

## <a name=dita-otAEM></a>Set new DITA-OT AEM instance 

* Upload Service pack-1 6.3.1 through CRXDE package manager.
* Upload com.adobe.fmdita-6.3-2.6.113.zip through CRXDE package manager.
* Restart AEM.

## <a name=mapTopics></a>Creating Maps and Topics

* Start AEM instance.
* Goto ASSETS >> Files >> Create DITA-OT Folder.
* Goto DITA-OT Folder >> Click on Create button.
* Select Maps and give the tile , name and then click on Done.
* Again click on Create button and then select Topic and give the title , name and click on Done.
* Write some data in topic.
* Hover on Maps and click on Edit button.
* Select the folder created and the drag maps first and then drag topics.
* Click on save button.

## <a name=pulginInstall></a>Installing Custom css pulgin in DITA-OT

* Check Dita-ot is install or not by writing `dita -version`in command prompt.
* Open command prompt and goto dita-ot path
* Type `dita -install <path of the pulgin>` inorder to install the pulgin.
* then type cd pulgin >> cd org.tekno.htmlcss 
* then type `ant -f customcss.xml`
* To check whether the build is successful or not type command `bin\dita -i <DITA-OT folder>\docsrc\samples\taskbook.ditamap -f customcss -o out/taskbook_output -v`
* Check the output in the `Out` folder created  and then delete the Output folder.
* Goto DITA-OT and zip the DITA-OT file.
* Upload it on the AEM in CRXDE in `/apps/<your_folder_name/DITA-OT.zip`.(For Uploading use Cyberduck tool).

## <a name=profile></a>Creating Profile

* Goto AEM navigation bar page.
* Click on the `Tools` on left panel.
* Select XML Documentation and Click on DITA Profiles.
* Select Default profile and then click on Edit button.
* Modify the path `AEM DITA OT output` to `/apps/<folder-name>/DITA-OT.zip`
* Click on the Done button. 

## <a name=output></a>Output of DITA-OT

* Goto Assets >> Files >> <Your_DITA-OT_folder> >> click on .map file that was created.
*  Select Custom and click on Edit.
*  Give the Transformation Name as `customcss`.
* Click on Done.
* Then select Custom checkbox and Click on Generate and then click on Output to see the output.
* Click on Custom to download the package.
* Unzip the package and then see the Output. 